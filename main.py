class Spell:
    def __init__(self, name: str, difficulty: int, category: str, damage: int, energy_spend: int):
        self.name = name
        if difficulty < 0:
            self.difficulty = 0
        elif difficulty > 100:
            self.difficulty = 100
        else:
            self.difficulty = difficulty        
        
        if damage < 0:
            self.category = 'restoration'
        elif damage > 0:
            self.category = 'attack'
        else:
            self.category = 'support'
        
        if damage <= -100:
            self.damage = -99
        elif damage >= 100:
            self.damage = 99
        else:
            self.damage = damage

        if energy_spend < 0:
            self.energy_spend = 0
        elif energy_spend > 100:
            self.energy_spend = 100
        else:
            self.energy_spend = energy_spend

class Sorcerer:
    def __init__(self, health_pool=100, energy_pool=100, spell_list=[]):
        if health_pool < 0:
            self.health_pool = 0
        elif health_pool > 100:
            self.health_pool = 100
        else:
            self.health_pool = health_pool
        
        if energy_pool < 0:
            self.energy_pool = 0
        elif energy_pool > 100:
            self.energy_pool = 100
        else:
            self.energy_pool = energy_pool

        self.spell_list = spell_list

    def learn_spell(self, spell: Spell):
        self.spell_list.append(spell)

    def conjuration(self, spell: Spell, adversary):
        self.spell = spell
        self.adversary = adversary

        for i in range(len(self.spell_list)):
            if spell == self.spell_list[i].name:
                spell = self.spell_list[i]
            else:
                return False

        if self.energy_pool < spell.energy_spend:
            return False

        if spell in self.spell_list:
            if self.energy_pool >= spell.energy_spend:
                self.energy_pool -= spell.energy_spend
                adversary.health_pool -= spell.damage

                return True

class Professor(Sorcerer):
    def __init__(self, name=str, class_name=str, is_teaching=bool):
        self.name = name
        self.class_name = class_name

    def teach(self):
        self.is_teaching = True

    def stop_teaching(self):
        self.is_teaching = False

    def __repr__(self):
        return '<class %s>' % self

class Student(Sorcerer):
    def __init__(self, name: str, school_year: int, age: int):
        self.name = name
        self.school_year = school_year 
        self.age = age
        self.class_list = []

    def add_class(self, class_name):
        return self.class_list.append(class_name)

    def attend_class(self):
        self.is_attending_class = True

    def stop_attend_class(self):
        self.is_attending_class = False

    def __repr__(self):
        return '<class %s>' % self

class Hogwarts:
    def __init__(self):
        self.professor_list = []
        self.student_list = []

    def dismiss(self, professor_name: str) -> bool:
        self.professor_name = professor_name

        if self.professor_list.remove(professor_name):
            return True

        return False

    def hire(self, professor: Professor):
        self.professor_list.append(professor.name)

    def kick_out(self, student_name: str) -> bool:
        self.student_name = student_name

        if self.student_list.remove(student_name):
            return True

        return False

    def enroll(self, student: Student):
        self.student_list.append(student.name)
